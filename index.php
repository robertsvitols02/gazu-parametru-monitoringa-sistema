<?php $page = "sakumlapa"; include('header.php'); ?>
<section class="admin">
    <div class="row">
        <div class="info">
            <div class="head-info">Sākumlapa</div>
            <form method='post'>
            <table>
                <tr>
                    <td>Sensoru rādījumi</td>
                </tr>
            </table>
            <?php
            require("connect_db.php");
            $lietotajiVaicajums = $savienojums->prepare('SELECT User_ID FROM users WHERE Username=?');
            $lietotajiVaicajums->bind_param("s", $_SESSION['username']);
            $lietotajiVaicajums->execute();
            $lietotajaDati = $lietotajiVaicajums->get_result()->fetch_assoc();
            $Lietotajs = $lietotajaDati ? $lietotajaDati['User_ID'] : null;

            $sensoruVaicajums = "SELECT S.Sensor_ID, S.Sensor_Name, G.Gas_Reading FROM Sensors as S JOIN Gas_Data as G ON G.ID_Sensor = S.Sensor_ID JOIN Rooms as R ON R.Room_ID = G.ID_Room WHERE R.ID_User = '$Lietotajs' ORDER BY S.Sensor_ID ASC";
            $result = $savienojums->query($sensoruVaicajums);

            $labels = [];
            $values = [];

            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $labels[] = $row['Sensor_Name'];
                    $values[] = $row['Gas_Reading'];
                }
            }

            $savienojums->close();
            ?>

            <style>
                #myChart {
                height: 500px;
                margin-left: auto;
                margin-right: auto;
                }
            </style>
            <canvas id="myChart"></canvas>
            <script>
            var ctx = document.getElementById('myChart').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: <?php echo json_encode($labels); ?>,
                    datasets: [{
                        label: 'Sensoru rādījums',
                        data: <?php echo json_encode($values); ?>,
                        backgroundColor: 'rgba(0, 123, 255, 0.5)',
                        borderColor: 'rgba(0, 123, 255, 1)',
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: false,
                    maintainAspectRatio: false,
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            </script>
        </div>
    </div>
</section>
<?php include('footer.php'); ?>
