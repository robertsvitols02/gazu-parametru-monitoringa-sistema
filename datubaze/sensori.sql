-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2023 at 09:54 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sensori`
--

-- --------------------------------------------------------

--
-- Table structure for table `alerts`
--

CREATE TABLE `alerts` (
  `Alert_ID` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `Message` varchar(150) NOT NULL,
  `ID_Room` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `alerts`
--

INSERT INTO `alerts` (`Alert_ID`, `Date`, `Message`, `ID_Room`) VALUES
(1, '2023-06-09 09:00:00', 'CO Detected', 2),
(2, '2023-06-09 09:00:00', 'Smoke Detected', 1),
(3, '2023-06-09 09:00:00', 'Methane Detected', 1),
(4, '2023-06-09 09:00:00', 'Alcohol Detected', 1),
(5, '2023-06-09 09:00:00', 'LPG Detected', 1),
(6, '2023-06-09 09:00:00', 'CO Detected', 1),
(7, '2023-06-09 09:00:00', 'Hydrogen Gas Detected', 1),
(8, '2023-06-09 09:00:00', 'Bad Air Quality Detected', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gas_data`
--

CREATE TABLE `gas_data` (
  `Data_ID` int(11) NOT NULL,
  `Date` datetime NOT NULL DEFAULT current_timestamp(),
  `ID_Sensor` int(11) NOT NULL,
  `Gas_Reading` int(11) DEFAULT NULL,
  `ID_Room` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `gas_data`
--

INSERT INTO `gas_data` (`Data_ID`, `Date`, `ID_Sensor`, `Gas_Reading`, `ID_Room`) VALUES
(1, '2023-06-09 09:00:00', 1, 333, 1),
(2, '2023-06-09 09:00:00', 2, 77, 1),
(3, '2023-06-09 09:00:00', 3, 54, 1),
(4, '2023-06-09 09:00:00', 4, 873, 1),
(5, '2023-06-09 13:00:00', 5, 414, 1),
(6, '2023-06-09 09:00:00', 6, 233, 1),
(7, '2023-06-09 09:00:00', 7, 107, 1),
(8, '2023-06-09 09:00:00', 8, 558, 1),
(9, '2023-06-09 09:00:00', 10, 981, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `Room_ID` int(11) NOT NULL,
  `Room_Number` varchar(45) NOT NULL,
  `Room_Name` varchar(150) NOT NULL,
  `ID_User` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`Room_ID`, `Room_Number`, `Room_Name`, `ID_User`) VALUES
(1, '427', 'Arduino', 6),
(2, '430', 'Laboratory', 6),
(5, '420', 'Math', 6);

-- --------------------------------------------------------

--
-- Table structure for table `sensors`
--

CREATE TABLE `sensors` (
  `Sensor_ID` int(11) NOT NULL,
  `Sensor_Name` varchar(100) NOT NULL,
  `Gas_Type` varchar(255) NOT NULL,
  `Connection_Type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `sensors`
--

INSERT INTO `sensors` (`Sensor_ID`, `Sensor_Name`, `Gas_Type`, `Connection_Type`) VALUES
(1, 'MQ-2', 'Methane, Butane, LPG, Smoke', 'Digital'),
(2, 'MQ-3', 'Alcohol, Ethanol, Smoke', 'Analog'),
(3, 'MQ-4', 'Methance, CNG, Gas', 'Analog'),
(4, 'MQ-5', 'Natural Gas, LPG', 'Analog'),
(5, 'MQ-6', 'LPG, Butane', 'Analog'),
(6, 'MQ-7', 'Carbon Monoxide', 'Digital'),
(7, 'MQ-8', 'Hydrogen Gas', 'Digital'),
(8, 'MQ-9', 'Carbon Monoxide, Gas', 'Digital'),
(10, 'MQ-135', 'Air Quality', 'Digital');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `User_ID` int(11) NOT NULL,
  `Username` varchar(45) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Admin` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`User_ID`, `Username`, `Password`, `Email`, `Admin`) VALUES
(6, 'admin', '$2y$10$4d1pwJEvOJ/lgQf36rdhT.xMlSk0ikd53tVXUyK2Jmaw10sc5d.0e', 'admin@admin.lv', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alerts`
--
ALTER TABLE `alerts`
  ADD PRIMARY KEY (`Alert_ID`),
  ADD KEY `fk_Alerts_Rooms1_idx` (`ID_Room`);

--
-- Indexes for table `gas_data`
--
ALTER TABLE `gas_data`
  ADD PRIMARY KEY (`Data_ID`),
  ADD KEY `fk_Gas_Data_Sensors_idx` (`ID_Sensor`),
  ADD KEY `fk_Gas_Data_Rooms1_idx` (`ID_Room`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`Room_ID`),
  ADD KEY `fk_Rooms_Users1_idx` (`ID_User`);

--
-- Indexes for table `sensors`
--
ALTER TABLE `sensors`
  ADD PRIMARY KEY (`Sensor_ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`User_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alerts`
--
ALTER TABLE `alerts`
  MODIFY `Alert_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `gas_data`
--
ALTER TABLE `gas_data`
  MODIFY `Data_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `Room_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sensors`
--
ALTER TABLE `sensors`
  MODIFY `Sensor_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `User_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `alerts`
--
ALTER TABLE `alerts`
  ADD CONSTRAINT `fk_Alerts_Rooms1` FOREIGN KEY (`ID_Room`) REFERENCES `rooms` (`Room_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `gas_data`
--
ALTER TABLE `gas_data`
  ADD CONSTRAINT `fk_Gas_Data_Rooms1` FOREIGN KEY (`ID_Room`) REFERENCES `rooms` (`Room_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Gas_Data_Sensors` FOREIGN KEY (`ID_Sensor`) REFERENCES `sensors` (`Sensor_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `fk_Rooms_Users1` FOREIGN KEY (`ID_User`) REFERENCES `users` (`User_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
