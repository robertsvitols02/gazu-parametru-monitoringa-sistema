<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Autorizācija portālā</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
</head>
<body>
    <div class="container">
        <div class="login">
            <div class="nosaukums">Autorizācija portālā</div>
                <div class="info">
                    <?php
                        if(isset($_POST["autorizacija"])){
                            require("connect_db.php");
                            session_start();

                            $Lietotajvards = mysqli_real_escape_string($savienojums, $_POST["lietotajs"]);
                            $Parole = mysqli_real_escape_string($savienojums, $_POST["parole"]);
                            $sqlVaicajums = "SELECT * FROM users WHERE Username = '$Lietotajvards' ";
                            $rezultats = mysqli_query($savienojums, $sqlVaicajums);

                            if (mysqli_num_rows($rezultats) == 1){
                                while($row = mysqli_fetch_array($rezultats)){
                                    if(password_verify($Parole, $row["Password"])){
                                        $_SESSION["username"] = $Lietotajvards;
                                        header("location:index.php");
                                    }else{
                                        echo "Nepareizs lietotājvārds vai parole!";
                                    }
                                }
                            }else{
                                echo "Kļūda sistēmā!";
                            }
                        }

                        if(isset($GET['logout'])){
                            session_destroy();
                        }
                    ?>
                </div>
            <form action="" method="post">
                <div class="row">
                    <i class="fas fa-user"></i>
                    <input type="text" name="lietotajs" placeholder="Lietotājvārds" required/>
                </div>
                <div class="row">
                    <i class="fas fa-lock"></i>
                    <input type="password" name="parole" placeholder="Parole" required/>
                </div>
                <div class="row poga">
                    <input type="submit" name="autorizacija" value="Ielogoties"/>
                </div>
            </form>
        </div>
    </div>
</body>
</html>