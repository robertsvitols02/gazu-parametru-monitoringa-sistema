<!DOCTYPE html>
<html lang="lv">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gāzes sensori</title>
    <link rel="stylesheet" href="style2.css?version=1">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>
<body>
    <header>
        <a href="index.php" class="logo">Gāzes sensori</a>
        <nav class="navbar">
            <a href="index.php" class="<?php echo($page == "sakumlapa" ? "active" : "");?>"><i class="far fa-address-card"></i> Sākumlapa</a>
            <a href="sensori.php" class="<?php echo($page == "sensori" ? "active" : "");?>"><i class="far fa-folder-open"></i> Mani sensori</a>
        </nav>

        <nav class="navbar">
            <?php
                session_start();
                if(isset($_SESSION['username'])){
                    require("connect_db.php");

                    $lietotajvardaVaicajums = $savienojums->prepare('SELECT Username FROM users WHERE Username=?');
                    $lietotajvardaVaicajums->bind_param("s", $_SESSION['username']);
                    $lietotajvardaVaicajums->execute();
                    $lietotajvards = $lietotajvardaVaicajums->get_result()->fetch_assoc();
                    $lietotajs = $lietotajvards ? $lietotajvards['Username'] : null;

                    echo "<span class='lietotajs'><i class='fas fa-user'></i>$lietotajs</span>";
                }else{
                    echo 'error';
                }
            ?>
            <a href="logout.php"><i class="fas fa-sign-out-alt"></i></a>
        </nav>
        <?php
                    if(isset($_SESSION['username'])){
                    }else{
                        echo "Tev šeit nav pieejas!";
                        header("location:login.php");
                    }
                ?>
    </header>