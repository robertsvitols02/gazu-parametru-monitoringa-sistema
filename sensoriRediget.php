<?php $page = "sensori"; include('header.php'); ?>
    <section class="admin">
        <div class="row">
            <div class="info">
                <div class="head-info">Sensoru rediģēšana</div>
                <?php
                    if($_SERVER['REQUEST_METHOD'] == 'POST'){
                        require("connect_db.php");
                        if(isset($_POST['rediget'])){
                            $sensoraNosaukums = $_POST['sensor_name'];
                            $sensoraTips = $_POST['sensor_type'];
                            $savienojumaTips = $_POST['connection_type'];

                            $atjaunotSensoruVaicajums = "UPDATE Sensors SET Sensor_Name = '$sensoraNosaukums', Gas_Type = '$sensoraTips', Connection_Type = '$savienojumaTips' WHERE Sensor_ID =".$_POST['rediget'];

                            if(mysqli_query($savienojums, $atjaunotSensoruVaicajums)){
                                echo "<div class='pieteiksanasKluda zals'>Sensors ir veiksmīgi atjaunots!</div>";
                                header("Refresh:1; url=sensori.php");
                            }else{
                                echo "<div class='pieteiksanasKluda sarkans'>Kļūda!</div>";
                                header("Refresh:1; url=sensori.php");
                            }
                        }else{
                            $sensoraID = $_POST['apskatit'];

                            $sensoraVaicajums = "SELECT * FROM Sensors WHERE Sensor_ID = $sensoraID";
                            $atlasaSensoru = mysqli_query($savienojums, $sensoraVaicajums) or die('Nekorekts vaicājums');

                            while($row = mysqli_fetch_assoc($atlasaSensoru)){
                                echo "
                                    <table class='noselect'>
                                        <form method='POST'>
                                        <tr><td class='main'>Sensora nosaukums</td><td class='value'><input type='text' name='sensor_name' value='{$row['Sensor_Name']}' class='box'></td></tr>
                                        <tr><td class='main'>Sensora tips</td><td class='value'><input type='text' name='sensor_type' value='{$row['Gas_Type']}' class='box'></td></tr>
                                        <tr><td class='main'>Savienojuma tips</td><td class='value'>
                                        <select name='connection_type' class='box2'>";
                                                    if ($row['Connection_Type'] == "Digital"){
                                                        echo "<option value='Digital' selected>Digital</option>";
                                                        echo "<option value='Analog'>Analog</option>";
                                                    }else{
                                                        echo "<option value='Analog' selected>Analog</option>";
                                                        echo "<option value='Digital'>Digital</option>";
                                                    }
                                            echo"
                                        </select>
                                        </td></tr>
                                    </table>
                                    <button type='submit' name='rediget' value='{$row['Sensor_ID']}' class='btn4'>Saglabāt</button>
                                    </from>
                                ";
                            }
                    }}else{
                        echo "<div class='pieteiksanasKluda sarkans'>Kaut kas nogāja greizi! Atgriezies <a class='alert-link' href='sensori.php'>iepriekšējā lapā</a> un mēģini vēlreiz!</div>";
                    }
                ?>
            </div>
        </div>
    </section>
<?php include('footer.php'); ?>
