<?php $page = "sensori"; include('header.php'); ?>
<section class="admin">
    <div class="row">
        <div class="info">
            <div class="head-info">Mani sensori</div>
            <form method='post'>
            <table>
                    <tr>
                        <th>Sensora nosaukums</th>
                        <th>Sensoru tips</th>
                        <th>Savienojuma tips</th>
                        <th>Rediģēt</th>
                    </tr>
                    <?php
                        require("connect_db.php");

                        $lietotajiVaicajums = $savienojums->prepare('SELECT User_ID FROM users WHERE Username=?');
                        $lietotajiVaicajums->bind_param("s", $_SESSION['username']);
                        $lietotajiVaicajums->execute();
                        $lietotajaDati = $lietotajiVaicajums->get_result()->fetch_assoc();
                        $Lietotajs = $lietotajaDati ? $lietotajaDati['User_ID'] : null;

                        $sensoruVaicajums = "SELECT S.Sensor_ID, S.Sensor_Name, S.Gas_Type, S.Connection_Type FROM Sensors as S JOIN Gas_Data as G ON G.ID_Sensor = S.Sensor_ID JOIN Rooms as R ON R.Room_ID = G.ID_Room WHERE R.ID_User = '$Lietotajs' ORDER BY S.Sensor_Name DESC";
                        $atlasaVisusSensorus = mysqli_query($savienojums, $sensoruVaicajums) or die("Nekorekts vaicājums!");

                        if(mysqli_num_rows($atlasaVisusSensorus) > 0){
                            while($row = mysqli_fetch_assoc($atlasaVisusSensorus)){
                                echo "
                                <tr>
                                    <td>{$row['Sensor_Name']}</td>
                                    <td>{$row['Gas_Type']}</td>
                                    <td>{$row['Connection_Type']}</td>
                                    <td>
                                        <form action='sensoriRediget.php' method='post'>
                                            <button type='submit' name='apskatit' value='{$row['Sensor_ID']}' class='btn2'><i class='fas fa-edit'></i>'</button>
                                        </form>
                                    </td>
                                </tr>";
                            }
                        }
                    ?>
                </table>
        </div>
    </div>
</section>
<?php include('footer.php'); ?>
