//#include <LiquidCrystal_PCF8574.h> lcd(0x27); // Adresācija LCD
#define Sensor1 A0 //Pirmais analogais 
#define Sensor2 A1 // Otrais analogais
#define Sensor3 A2 // Trešais analogais
#define Sensor4 A3 // Ceturtais analogais
#define Sensor5 A4 // Piektais analogais
#define DigitalSensor 8 // Pirmais digitālais
#define DigitalSensor2 7 // Otrais digitālais
#define DigitalSensor3 6 // Trešais digitālais
#define DigitalSensor4 5 // Ceturtais digitālais

int A, B, Aa,Ab ,Ac, Ba, Bb, Bc ; // Mainīgie funckijām, lai aprēķinātu lietas, kā izmaiņas, utt.
struct SensorNode { // Saistītā saraksta struktūra
  int SensorData; // Skaitļa  mainīgais
  SensorNode* next; // Norāde uz nākamo
};

SensorNode* headSensor1 = NULL; //Tukšs saraksts
SensorNode* headSensor2 = NULL; //Tukšs saraksts
SensorNode* headSensor3 = NULL; //Tukšs saraksts
SensorNode* headSensor4 = NULL; //Tukšs saraksts
SensorNode* headSensor5 = NULL; //Tukšs saraksts

int countSensorReadings(SensorNode* head) { // Lasījumu saskaitīšanas funkcija
  int count = 0; // Skaita mainīgais
  SensorNode* current = head; // Esošā norāde 
  while (current != NULL) { // Kāmēram esošais sarakstā nav NULLE
    count++; // Pieskaita skaita mainīgajam
    current = current->next; // Iet uz nākamo sarakstā
  }
  return count; // Atgriež skaitu
}

void insertSensorReading(SensorNode** head, int data) { // Nolasījuma ievietošanas funkcija
  SensorNode* newNode = new SensorNode(); // Jauns saraksts izveidots
  newNode->SensorData = data; // Ievieto sarakstā datus
  newNode->next = NULL; // Nākamais aiz datiem ir tukšais

  if (*head == NULL) { // Ja pirmā vērtība ir tukša
    *head = newNode;  // Izveido jaunu vērtību
    return; // Atgriež to
  }

  if (countSensorReadings(*head) >= 10) { // Izsauc funkciju un pārbaud vai ir vismaz 10 rādījumu
    SensorNode* current = *head; // Norāde uz sākumu ar tagadējo
    while (current->next->next != NULL) { // Kāmēram nākamie nav NULLE
      current = current->next; // Esošais aiziet tālāk
    }
    delete current->next; // Izdzēš nākamo
    current->next = NULL; // NULLES beigu vērtība
  }

  newNode->next = *head;
  *head = newNode;
}

int calculateMedianReadings(SensorNode* head) { // Mediānas aprēķināšanas funkcija
  int subsetSize = countSensorReadings(head) / 10; // Izdala ar 10, lai iegūtu vidējo

  SensorNode* current = head; 
  int count = 0; // Skaita mainīgais
  int subsetCount = 0;
  int subsetSum = 0; // Summas mainīgais 

  while (current != NULL) { // Kāmēram nav NULLES norāde
    subsetSum += current->SensorData; // Iet uz nākamo datu 
    count++; // Pieskaita skaita mainīgajam

    if (count % subsetSize == 0) {
      int median = subsetSum / subsetSize;

      subsetCount++;
      subsetSum = 0;

      if (subsetCount == 5) {
        return median;  // Atgriež mediānu
      }
    }

    current = current->next; // Aiziet uz nākamo sarakstā
  }
  
  return -1;  // Atgriež -1, ja nav mediānas
}
// Readings for Sensor1
int Median1 = 0; // Tukšā mediāna funkcijai
void readSensor1() { // Sensora nolasījuma funkcija
  int sensorReading = analogRead(Sensor1); // Analogā lasīšana sensoram
  insertSensorReading(&headSensor1, sensorReading); // Ievieto savā saistītā sarakstā

  if (countSensorReadings(headSensor1) >= 10) { // Kad sasniedz 10 lasījumus
    Median1 = calculateMedianReadings(headSensor1); // Izsauc mediānas aprēķināšanas funkciju
  }
}

// Readings for Sensor2
int Median2 = 0; // Tukšā mediāna funkcijai
void readSensor2() { // Sensora nolasījuma funkcija
  int sensorReading = analogRead(Sensor2); // Analogā lasīšana sensoram
  insertSensorReading(&headSensor2, sensorReading); // Ievieto savā saistītā sarakstā

  if (countSensorReadings(headSensor2) >= 10) { // Kad sasniedz 10 lasījumus
    Median2 = calculateMedianReadings(headSensor2); // Izsauc mediānas aprēķināšanas funkciju
  }
}

// Readings for Sensor3
int Median3 = 0; // Tukšā mediāna funkcijai
void readSensor3() { // Sensora nolasījuma funkcija
  int sensorReading = analogRead(Sensor3); // Analogā lasīšana sensoram
  insertSensorReading(&headSensor3, sensorReading); // Ievieto savā saistītā sarakstā

  if (countSensorReadings(headSensor3) >= 10) { // Kad sasniedz 10 lasījumus
    Median3 = calculateMedianReadings(headSensor3); // Izsauc mediānas aprēķināšanas funkciju
  }
}

// Readings for Sensor4
int Median4 = 0; // Tukšā mediāna funkcijai
void readSensor4() { // Sensora nolasījuma funkcija
  int sensorReading = analogRead(Sensor4); // Analogā lasīšana sensoram
  insertSensorReading(&headSensor4, sensorReading); // Ievieto savā saistītā sarakstā

  if (countSensorReadings(headSensor4) >= 10) { // Kad sasniedz 10 lasījumus
    Median4 = calculateMedianReadings(headSensor4); // Izsauc mediānas aprēķināšanas funkciju
  }
}
void Prediction(){

delay(500); // Pagaida, lai iegūtu citu nolasījumu
readSensor1();
B = Median1; // Nolasījums ciklā
/*int T = (650 - A/tan(B-A)) * 0.5; // Pareģošanas izteiksme
//lcd.clear()
if(T<=0){ // Ja nolasījumi neaug
//lcd.setCursor(0, 1);
Serial.println("Viss ir labi!");}
else{ // Ja nolasījumi pieaug
//setCursor(0, 1);
Serial.print("Gāzes būs pār daudz pēc: ");
Serial.print(T);
Serial.println(" sekundēm");
}*/
A=B;
int C = A-B; // Vērtību maiņai
//lcd.setCursor(0, 0);
//Serial.print("Gāzes daudzums šobrīd: ");
Serial.print("*G");
Serial.print(B);
Serial.println("*");
Serial.print("*T");
Serial.print(B);
Serial.println("*");

}
void Prediction1(){

delay(500); // Pagaida, lai iegūtu citu nolasījumu
readSensor1();
B = Median1; // Nolasījums ciklā
int T = (650 - A/tan(B-A)) * 0.5; // Pareģošanas izteiksme
//lcd.clear()
if(T<=0){ // Ja nolasījumi neaug
//lcd.setCursor(0, 1);
Serial.println("Viss ir labi!");}
else{ // Ja nolasījumi pieaug
//setCursor(0, 1);
Serial.print("Gāzes būs pār daudz pēc: ");
Serial.print(T);
Serial.println(" sekundēm");
}
A=B;
int C = A-B; // Vērtību maiņai
//lcd.setCursor(0, 0);
Serial.print("Gāzes daudzums šobrīd: ");
Serial.println(B);

}
void Prediction2(){

delay(500); // Pagaida, lai iegūtu citu nolasījumu
readSensor2();
Ba = Median2; // Nolasījums ciklā
int Ta = (650 - Aa/tan(Ba-Aa)) * 0.5; // Pareģošanas izteiksme
//lcd.clear()
if(Ta<=0){ // Ja nolasījumi neaug
//lcd.setCursor(0, 1);
Serial.println("Viss ir labi!");}
else{ // Ja nolasījumi pieaug
//setCursor(0, 1);
Serial.print("Gāzes būs pār daudz pēc: ");
Serial.print(Ta);
Serial.println(" sekundēm");
}
Aa=Ba;
int Ca = Aa-Ba; // Vērtību maiņai
//lcd.setCursor(0, 0);
Serial.print("Gāzes daudzums šobrīd: ");
Serial.println(Ba);

}
void Prediction3(){

delay(500); // Pagaida, lai iegūtu citu nolasījumu
readSensor3();
Bb = Median3; // Nolasījums ciklā
int Tb = (650 - Ab/tan(Bb-Ab)) * 0.5; // Pareģošanas izteiksme
//lcd.clear()
if(Tb<=0){ // Ja nolasījumi neaug
//lcd.setCursor(0, 1);
Serial.println("Viss ir labi!");}
else{ // Ja nolasījumi pieaug
//setCursor(0, 1);
Serial.print("Gāzes būs pār daudz pēc: ");
Serial.print(Tb);
Serial.println(" sekundēm");
}
Ab=Bb;
int Cb = Ab-Bb; // Vērtību maiņai
//lcd.setCursor(0, 0);
Serial.print("Gāzes daudzums šobrīd: ");
Serial.println(Bb);

}
void Prediction4(){

delay(500); // Pagaida, lai iegūtu citu nolasījumu
readSensor4();
Bc = Median4; // Nolasījums ciklā
int Tc = (650 - Ac/tan(Bc-Ac)) * 0.5; // Pareģošanas izteiksme
//lcd.clear()
if(Tc<=0){ // Ja nolasījumi neaug
//lcd.setCursor(0, 1);
Serial.println("Viss ir labi!");}
else{ // Ja nolasījumi pieaug
//setCursor(0, 1);
Serial.print("Gāzes būs pār daudz pēc: ");
Serial.print(Tc);
Serial.println(" sekundēm");
}
Ac=Bc;
int Cc = Ac-Bc; // Vērtību maiņai
//lcd.setCursor(0, 0);
Serial.print("Gāzes daudzums šobrīd: ");
Serial.println(Bc);

}

void setup() {
Serial.begin(9600);
pinMode(DigitalSensor, INPUT); // Pieslēdz digitāli pieslēgto sensoru
//lcd.begin(40, 2); // Izmērs LCD - Kolonnas un Rindas
//lcd.setBacklight(100); // Gaisma LCD
readSensor1();
A = Median1; // Nolasījums pirms cikla
readSensor2();
Aa = Median2; // Nolasījums pirms cikla
readSensor3();
Ab = Median3; // Nolasījums pirms cikla
readSensor4();
Ac = Median4; // Nolasījums pirms cikla
}


void loop() {

Prediction();

/*
int SensVal = digitalRead(DigitalSensor); // Digitāli pieslēgts sensors

if (SensVal == HIGH){ // Kad digitāli pieslēgts sensors jūt pārāk daudz gāzes
Serial.println("Digitālā gāze sajusta!");

}else{// Kad jūt pietiekami maz
  Serial.println("Digitālās gāzes nav!");
}
*/
}
