<!DOCTYPE html>
<html lang="lv">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Autorizācija portālā</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
</head>
<body>
    <div class="container">
        <div class="login">
            <div class="nosaukums">Reģistrācija portālā</div>
                <div class="info">
                    <?php
                        if(isset($_POST["registracija"])){
                            $Lietotajs = $_POST['lietotajs'];
                            $Epasts = $_POST['epasts'];
                            $Parole = $_POST['parole'];
                            $Parole2 = $_POST['parole2'];

                            if (!empty($Lietotajs) && !empty($Epasts) && !empty($Parole) && !empty($Parole2)){
                                if (filter_var($Epasts, FILTER_VALIDATE_EMAIL)) {
                                    if($Parole == $Parole2){
                                        require("connect_db.php");
                                        $sqlVaicajums = "SELECT * FROM users WHERE Username = '$Lietotajs' ";
                                        $rezultats = mysqli_query($savienojums, $sqlVaicajums);
                                        if (!mysqli_num_rows($rezultats) == 1){
                                            $parole_hashota = password_hash($Parole2, PASSWORD_DEFAULT);
                                            $ievietot = "INSERT INTO users(Username, Password, Email) VALUE ('$Lietotajs', '$parole_hashota', '$Epasts')";
                                            if(mysqli_query($savienojums, $ievietot)){
                                                echo "<span style='color:#AFE1AF;'>Reģistrācija veiksmīga!</span>";
                                                header("Refresh:0.6; url=index.php");
                                            }else{
                                                echo "Kļūda: ".$ievietot."<br>".mysqli_error($savienojums);
                                            }
                                        }else{
                                            echo "Šāds lietotājvārds jau eksistē!";
                                        }
                                    }else{
                                        echo "Parole nav ierakstīta vienādi abos laukos!";
                                    }
                                }else{
                                    echo "Nepareizs epasta formāts!";
                                }
                            }else{
                                echo "Visi lauki nav aizpildīti!";
                            }
                        }
                    ?>
                </div>
            <form action="" method="post">
                <div class="row">
                    <i class="fas fa-user"></i>
                    <input type="text" name="lietotajs" placeholder="Lietotājvārds" required/>
                </div>
                <div class="row">
                    <i class="fas fa-envelope"></i>
                    <input type="text" name="epasts" placeholder="E-pasts" required/>
                </div>
                <div class="row">
                    <i class="fas fa-lock"></i>
                    <input type="password" name="parole" placeholder="Parole" required/>
                </div>
                <div class="row">
                    <i class="fas fa-lock"></i>
                    <input type="password" name="parole2" placeholder="Parole (atkārtoti)" required/>
                </div>
                <div class="row poga">
                    <input type="submit" name="registracija" value="Reģistrēties"/>
                </div>
                <div class="registreties">Es jau reģistrējies? <a href="index.php">Ielogojies!</a></div>
            </form>
        </div>
    </div>
</body>
</html>